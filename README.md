# Mdock
Basic openbox settings and dock application

# For the app to work properly

## mdock base packages
```
python-gobject, libwnck, python-psutil, python-iwlib
```
python-gobject -> create mdock and msettings window  
libwnck -> mdock task work  
python-psutil -> battery work  
python-iwlib -> wifi work  

## mdock system packages
```
openbox, feh, pavucontrol, connman, stalonetray
```
openbox -> mdock and msettigns testing and work openbox  
feh -> msettigns change background feh  
pavucontrol -> pulseaudio -> pactl sound change commands, and open mixer pavucontrol or pavucontrol-qt  
connman -> connman network system and open network connman-gtk or cmst  
stalonetray -> systemtray controls

# How to install
```
sudo python3 setup.py install
```

# ScreenShots
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/1.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/2.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/3.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/4.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/5.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/6.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/7.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/8.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/9.png)
![](https://gitlab.com/sonakinci41/mdock/-/raw/master/screenshots/10.png)
