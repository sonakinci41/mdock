import xml.etree.ElementTree as ET
import os


#f = open(os.path.expanduser("~/.config/openbox/rc.xml"),"r")
#root = ET.fromstring(f.read())
#f.close()

def get_ns(root):
	namespace = root.tag.split("}")[0][1:]
	ns = {'real_person': namespace}
	return (namespace,ns)


def write_css_file(root):
	tree = ET.ElementTree(root)
	ET.register_namespace('', get_ns(root)[0])
	tree.write(open(os.path.expanduser("~/.config/openbox/rc.xml"), 'w'), encoding='unicode')
	os.system("openbox --restart &")


def get_resistance(root):
	ns = get_ns(root)[1]
	resistance = root.find('real_person:resistance', ns)
	strength = resistance.find('real_person:strength', ns)#strength Tells Openbox how much resistance (in pixels) there is between two windows before it lets them overlap.
	screen_edge_strength = resistance.find('real_person:screen_edge_strength', ns)#screen_edge_strength Basically the same as strength but between window and the screen edge.
	return {"strength":strength.text,"screen_edge_strength":screen_edge_strength.text}


def set_resistance(root,g_strength, g_screen_edge_strength):
	ns = get_ns(root)[1]
	resistance = root.find('real_person:resistance', ns)
	strength = resistance.find('real_person:strength', ns)
	screen_edge_strength = resistance.find('real_person:screen_edge_strength', ns)
	strength.text = g_strength
	screen_edge_strength.text = g_screen_edge_strength
	write_css_file(root)


def get_focus(root):
	ns = get_ns(root)[1]
	focus = root.find('real_person:focus', ns)
	focusNew = focus.find('real_person:focusNew', ns)#Openbox will automatically give focus to new windows when they are created, otherwise the focus will stay as it is.
	focusLast = focus.find('real_person:focusLast', ns)#Makes focus follow mouse. e.g. when the mouse is being moved the focus will be given to window under the mouse cursor.
	followMouse = focus.find('real_person:followMouse', ns)#When switching desktops, focus the last focused window on that desktop again, regardless of where the mouse is. Only applies followMouse is set.
	focusDelay = focus.find('real_person:focusDelay', ns)#The time (in milliseconds) Openbox will wait before giving focus to the window under mouse cursor. Only applies if followMouse is set.
	underMouse = focus.find('real_person:underMouse', ns)#Focus windows under the mouse not only when the mouse moves, but also when it enters another window due to other reasons (e.g. the window the mouse was in moved/closed/iconified etc). Only applies if followMouse is set.
	raiseOnFocus = focus.find('real_person:raiseOnFocus', ns)#Also raises windows to top when they are focused. Only applies if followMouse is set.
	return {"focusNew":focusNew.text,"focusLast":focusLast.text,"followMouse":followMouse.text,
				"focusDelay":focusDelay.text,"underMouse":underMouse.text,"raiseOnFocus":raiseOnFocus.text}


def set_focus(root,g_focus_new,g_focus_last,g_follow_mouse,g_focus_delay,g_under_mouse,g_raise_on_focus):
	ns = get_ns(root)[1]
	focus = root.find('real_person:focus', ns)
	focusNew = focus.find('real_person:focusNew', ns)
	focusLast = focus.find('real_person:focusLast', ns)
	followMouse = focus.find('real_person:followMouse', ns)
	focusDelay = focus.find('real_person:focusDelay', ns)
	underMouse = focus.find('real_person:underMouse', ns)
	raiseOnFocus = focus.find('real_person:raiseOnFocus', ns)
	focusNew.text = g_focus_new
	focusLast.text = g_focus_last
	followMouse.text = g_follow_mouse
	focusDelay.text = g_focus_delay
	underMouse.text = g_under_mouse
	raiseOnFocus.text = g_raise_on_focus
	write_css_file(root)


def get_placement(root):
	ns = get_ns(root)[1]
	placement = root.find('real_person:placement', ns)
	policy = placement.find('real_person:policy', ns)#policy can be either Smart or UnderMouse.
	#Smart will cause new windows to be placed automatically by Openbox.
	#UnderMouse makes new windows to be placed under the mouse cursor.
	center = placement.find('real_person:center', ns)#center can be either yes or no. If it is enabled, windows will open centered in the free area found.
	monitor = placement.find('real_person:monitor', ns)#with Smart placement on a multi-monitor system, try to place new windows
	#on: 'Any' - any monitor, 'Mouse' - where the mouse is, 'Active' - where
	#the active window is, 'Primary' - only on the primary monitor
	primaryMonitor = placement.find('real_person:primaryMonitor', ns)#The monitor where Openbox should place popup dialogs such as the
	#focus cycling popup, or the desktop switch popup.  It can be an index
	#from 1, specifying a particular monitor.  Or it can be one of the
	#following: 'Mouse' - where the mouse is, or
	#'Active' - where the active window is
	return {"policy":policy.text,"center":center.text,"monitor":monitor.text,"primaryMonitor":primaryMonitor.text}

def set_placement(root,g_policy,g_center,g_monitor,g_primaryMonitor):
	ns = get_ns(root)[1]
	placement = root.find('real_person:placement', ns)
	policy = placement.find('real_person:policy', ns)
	center = placement.find('real_person:center', ns)
	monitor = placement.find('real_person:monitor', ns)
	primaryMonitor = placement.find('real_person:primaryMonitor', ns)
	policy.text = g_policy
	center.text = g_center
	monitor.text = g_monitor
	primaryMonitor.text = g_primaryMonitor
	write_css_file(root)

def get_theme(root):
	ns = get_ns(root)[1]
	theme = root.find('real_person:theme', ns)
	name = theme.find('real_person:name', ns)#name The name of the Openbox theme to use.
	titleLayout = theme.find('real_person:titleLayout', ns)#titleLayout tells in which order and what buttons should be in a window's titlebar. The following letters can be used, each only once.
	# N :window icon # L :window label (aka. title) # I: iconify
	# M: maximize # C: close # S: shade (roll up/down) # D: omnipresent (on all desktops).
	keepBorder = theme.find('real_person:keepBorder', ns)#keepBorder tells if windows should keep the border drawn by Openbox when window decorations are turned off.
	animateIconify = theme.find('real_person:animateIconify', ns)#animateIconify adds a little iconification animation if enabled. font Specifies the font to use for a specific element of the window. Place can be either of
	fonts = theme.findall('real_person:font', ns)
	find_fonts = []
	#This Places
	#ActiveWindow: Title bar of the active window
	#InactiveWindow: Title bar of the inactive window
	#MenuHeader: Titles in the menu
	#MenuItem: Items in the menu
	#OnScreenDisplay: Text in popups such as window cycling or desktop switching popups
	for font in fonts:
		f_place = font.attrib["place"]
		f = {"place":f_place}
		f_name = font.find('real_person:name', ns)
		f["name"] = f_name.text
		f_size = font.find('real_person:size', ns)
		f["size"] = f_size.text
		f_weight = font.find('real_person:weight', ns)
		f["weight"] = f_weight.text
		f_slant = font.find('real_person:slant', ns)
		f["slant"] = f_slant.text
		find_fonts.append(f)
	return {"name":name.text,"titleLayout":titleLayout.text,
			"keepBorder":keepBorder.text,"animateIconify":animateIconify.text,
			"fonts":find_fonts}

def set_theme(root,g_name,g_titleLayout,g_keepBorder,g_animateIconify,g_fonts):
	ns = get_ns(root)[1]
	theme = root.find('real_person:theme', ns)
	name = theme.find('real_person:name', ns)
	name.text = g_name
	titleLayout = theme.find('real_person:titleLayout', ns)
	titleLayout.text = g_titleLayout
	keepBorder = theme.find('real_person:keepBorder', ns)
	keepBorder.text = g_keepBorder
	animateIconify = theme.find('real_person:animateIconify', ns)
	animateIconify.text = g_animateIconify
	fonts = theme.findall('real_person:font', ns)
	for font in fonts:
		f_place = font.attrib
		f_name = font.find('real_person:name', ns)
		f_name.text = g_fonts[f_place['place']]["name"]
		f_size = font.find('real_person:size', ns)
		f_size.text = g_fonts[f_place['place']]["size"]
	write_css_file(root)


def get_desktop(root):
	ns = get_ns(root)[1]
	desktop = root.find('real_person:desktops', ns)
	number = desktop.find('real_person:number', ns)#number The number of virtual desktops to use.
	firstdesk = desktop.find('real_person:firstdesk', ns)#firstdesk The number of the desktop to use when first started.
	popupTime = desktop.find('real_person:popupTime', ns)#popupTime Time (in milliseconds) to show the popup when switching desktops. Can be set to 0 to disable the popup completely.
	names = desktop.find('real_person:names', ns)#names Each name tag names your desktops, in ascending order. Unnamed desktops will be named automatically depending on the locale. You can name more desktops than specified in number if you want.
	find_names = []
	for name in names.findall('real_person:name', ns):
		find_names.append(name.text)
	return {"number":number.text,"firstdesk":firstdesk.text,"popupTime":popupTime.text,
			"names":find_names}

def set_desktop(root,g_number,g_firstdesk,g_popupTime,g_names):
	ns = get_ns(root)[1]
	desktop = root.find('real_person:desktops', ns)
	number = desktop.find('real_person:number', ns)
	number.text = g_number
	firstdesk = desktop.find('real_person:firstdesk', ns)
	firstdesk.text = g_firstdesk
	popupTime = desktop.find('real_person:popupTime', ns)
	popupTime.text = g_popupTime

	names = desktop.find('real_person:names', ns)
	names.clear()
	names = desktop.find('real_person:names', ns)
	for n in g_names:
		a = ET.SubElement(names,"name")
		a.text = n
	write_css_file(root)


def get_resize(root):
	ns = get_ns(root)[1]
	resize = root.find('real_person:resize', ns)
	drawContents = resize.find('real_person:drawContents', ns)#Resize the program inside the window while resizing. When disabled the unused space will be filled with a uniform color during a resize.
	popupShow = resize.find('real_person:popupShow', ns)#When to show the move/resize popup. Always always shows it, Never never shows it, Nonpixel shows it only when resizing windows that have specified they are resized in increments larger than one pixel, usually terminals.
	popupPosition = resize.find('real_person:popupPosition', ns)#Where to show the popup.
	#Top shows the popup above the titlebar of the window.
	#Center shows it centered on the window.
	#Fixed shows it in a fixed location on the screen specified by popupFixedPosition.
	popupFixedPosition = resize.find('real_person:popupFixedPosition', ns)#Specifies where on the screen to show the position when Fixed. Both x and y take coordinates as described here.
	pos_x = popupFixedPosition.find('real_person:x', ns)
	pos_y = popupFixedPosition.find('real_person:y', ns)
	return {"drawContents":drawContents.text,"popupShow":popupShow.text,
			"popupPosition":popupPosition.text,"pos_x":pos_x.text,"pos_y":pos_y.text}

def set_resize(root,g_drawContents,g_popupShow,g_popupFixedPosition,g_pos_x,g_pos_y):
	ns = get_ns(root)[1]
	resize = root.find('real_person:resize', ns)
	drawContents = resize.find('real_person:drawContents', ns)
	drawContents.text = g_drawContents
	popupShow = resize.find('real_person:popupShow', ns)
	popupShow.text = g_popupShow
	popupFixedPosition = resize.find('real_person:popupFixedPosition', ns)
	popupFixedPosition.text = g_popupFixedPosition
	pos_x = popupFixedPosition.find('real_person:x', ns)
	pos_x.text = g_pos_x
	pos_y = popupFixedPosition.find('real_person:y', ns)
	pos_y.text = g_pos_y
	write_css_file(root)


def get_keyboard(root):
	ns = get_ns(root)[1]
	keyboards = {}
	keyboard = root.find('real_person:keyboard', ns)
	for keybind in keyboard.findall('real_person:keybind', ns):
		key_name = keybind.attrib["key"]
		actions = keybind.findall('real_person:action', ns)
		for action in actions:
			action_t = get_action(action,root)
			keyboards[key_name] = action_t
	print(keyboard.findall('real_person:keybind', ns))


def get_action(action,root):
	action_name = action.attrib["name"]
	if action_name == "Execute":
		return {action_name:get_action_execute(action,root)}
		

def get_action_execute(action,root):
	ns = get_ns(root)[1]
	command = action.find('real_person:command', ns)#A string which is the command to be executed, along with any arguments to be passed to it. The "~" tilde character will be expanded to your home directory, but no other shell expansions or scripting syntax may be used in the command unless they are passed to the sh command. Also, the & character must be written as &amp; in order to be parsed correctly. <execute> is a deprecated name for <command>.
	prompt = action.find('real_person:prompt', ns)#A string which Openbox will display in a popup dialog, along with "Yes" and "No" buttons. The execute action will only be run if you choose the "Yes" button in the dialog.
	notify = action.find('real_person:startupnotify', ns)
	if notify != None:
		enabled = notify.find('real_person:enabled', ns)#A boolean (yes/no) which says if the startup notification protocol should be used to notify other programs that an application is launching. This is disabled by default to avoid it being used for old-style xterminals.
		wmclass = notify.find('real_person:wmclass', ns)#A string specifying one of the values that will be in the application window's WM_CLASS property when the window appears. This is not needed for applications that support the startup-notification protocol.
		name = notify.find('real_person:name', ns)#The name of the application which is launching. If this option is not used, then the command itself will be used for the name.
		icon = notify.find('real_person:icon', ns)#The icon of the application which is launching. If this option is not used, then the command itself will be used to pick the icon.
		notify = {"enabled":is_none(enabled,"no"),"wmclass":is_none(wmclass,"none"),
				"name":is_none(name,"none"),"icon":is_none(icon,"none")}
	else:
		notify = {}
	return {"command":is_none(command,""),"prompt":is_none(prompt,"none"),
			"startupnotify":notify}

def get_margins(root):
	ns = get_ns(root)[1]
	margins = root.find('real_person:margins', ns)
	top = margins.find('real_person:top', ns)
	bottom = margins.find('real_person:bottom', ns)
	left = margins.find('real_person:left', ns)
	right = margins.find('real_person:right', ns)
	return {"top":top.text,"bottom":bottom.text,"left":left.text,"right":right.text}


def set_margins(root,g_top,g_bottom,g_left,g_right):
	ns = get_ns(root)[1]
	margins = root.find('real_person:margins', ns)
	top = margins.find('real_person:top', ns)
	top.text = g_top
	bottom = margins.find('real_person:bottom', ns)
	bottom.text = g_bottom
	left = margins.find('real_person:left', ns)
	left.text = g_left
	right = margins.find('real_person:right', ns)
	right.text = g_right
	write_css_file(root)


def get_menu(root):
	ns = get_ns(root)[1]
	menu = root.find('real_person:menu', ns)
	hideDelay = menu.find('real_person:hideDelay', ns)
	middle = menu.find('real_person:middle', ns)
	submenuShowDelay = menu.find('real_person:submenuShowDelay', ns)
	submenuHideDelay = menu.find('real_person:submenuHideDelay', ns)
	applicationIcons = menu.find('real_person:applicationIcons', ns)
	if applicationIcons == None:
		applicationIcons = menu.find('real_person:showIcons', ns)
	manageDesktops = menu.find('real_person:manageDesktops', ns)
	file_ = menu.find('real_person:file', ns)
	return {"hideDelay":hideDelay.text,"middle":middle.text,"file":file_.text,
			"submenuHideDelay":submenuHideDelay.text,"submenuShowDelay":submenuShowDelay.text,
			"applicationIcons":applicationIcons.text,"manageDesktops":manageDesktops.text}


def set_menu(root,g_hideDelay,g_middle,g_submeShow,g_submeHide,g_appicon,g_manage_desktop,g_file):
	ns = get_ns(root)[1]
	menu = root.find('real_person:menu', ns)
	hideDelay = menu.find('real_person:hideDelay', ns)
	hideDelay.text = g_hideDelay
	middle = menu.find('real_person:middle', ns)
	middle.text = g_middle
	submenuShowDelay = menu.find('real_person:submenuShowDelay', ns)
	submenuShowDelay.text = g_submeShow
	submenuHideDelay = menu.find('real_person:submenuHideDelay', ns)
	submenuHideDelay.text = g_submeHide
	applicationIcons = menu.find('real_person:applicationIcons', ns)
	if applicationIcons == None:
		applicationIcons = menu.find('real_person:showIcons', ns)
	applicationIcons.text = g_appicon
	manageDesktops = menu.find('real_person:manageDesktops', ns)
	manageDesktops.text = g_manage_desktop
	file_ = menu.find('real_person:file', ns)
	file_.text = g_file
	write_css_file(root)


def get_dock(root):
	ns = get_ns(root)[1]
	dock = root.find('real_person:dock', ns)
	position = dock.find('real_person:position', ns)
	stacking = dock.find('real_person:stacking', ns)
	direction = dock.find('real_person:direction', ns)
	floatingX = dock.find('real_person:floatingX', ns)
	floatingY = dock.find('real_person:floatingY', ns)
	autoHide = dock.find('real_person:autoHide', ns)
	hideDelay = dock.find('real_person:hideDelay', ns)
	showDelay = dock.find('real_person:showDelay', ns)
	moveButton = dock.find('real_person:moveButton', ns)
	noStrut = dock.find('real_person:noStrut', ns)
	return {"position":position.text,"stacking":stacking.text,
			"direction":direction.text,"floatingX":floatingX.text,
			"floatingY":floatingY.text,"autoHide":autoHide.text,
			"hideDelay":hideDelay.text,"showDelay":showDelay.text,
			"moveButton":moveButton.text,"noStrut":noStrut.text}


def set_dock(root,g_position,g_stacking,g_direction,g_floatingX,g_floatingY,g_autoHide,g_hideDelay,g_showDelay,g_moveButton,g_noStrut):
	ns = get_ns(root)[1]
	dock = root.find('real_person:dock', ns)
	position = dock.find('real_person:position', ns)
	position.text = g_position
	stacking = dock.find('real_person:stacking', ns)
	stacking.text = g_stacking
	direction = dock.find('real_person:direction', ns)
	direction.text = g_direction
	floatingX = dock.find('real_person:floatingX', ns)
	floatingX.text = g_floatingX
	floatingY = dock.find('real_person:floatingY', ns)
	floatingY.text = g_floatingY
	autoHide = dock.find('real_person:autoHide', ns)
	autoHide.text = g_autoHide
	hideDelay = dock.find('real_person:hideDelay', ns)
	hideDelay.text = g_hideDelay
	showDelay = dock.find('real_person:showDelay', ns)
	showDelay.text = g_showDelay
	moveButton = dock.find('real_person:moveButton', ns)
	moveButton.text = g_moveButton
	noStrut = dock.find('real_person:noStrut', ns)
	noStrut.text = g_noStrut
	write_css_file(root)



def is_none(control,default):
	if control == None:
		return default
	else:
		return control.text


#print(get_resistance())
#print(get_focus())
#print(get_placement())
#print(get_theme())
#print(get_desktop())
#print(get_resize())
#print(get_keyboard())



"""
for child in root:
	print(child.tag, child.attrib)
	ch_tag = child.getroot()
	print(ch_tag)"""