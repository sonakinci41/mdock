import gi, os, subprocess
import xml.etree.ElementTree as ET
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk, GdkPixbuf
from m_settings import xml_setting

class Settings(Gtk.ScrolledWindow):
	def __init__(self,parent):
		Gtk.Window.__init__(self)
		self.parent = parent
		self.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		#main_box = Gtk.VBox()
		#self.add(main_box)

		self.name = "OpenBox"
		try:
			self.d_icon_theme = Gtk.IconTheme.get_default()
			self.icon = self.d_icon_theme.load_icon("openbox",32,Gtk.IconLookupFlags.FORCE_REGULAR)
		except:
			self.icon = None
		self.title = "Openbox Settings"

		#Openbox dir exists control
		rc_dir = os.path.expanduser("~/.config/openbox/rc.xml")
		if not os.path.exists(rc_dir):
			if not os.path.exists("/etc/xdg/openbox/rc.xml"):
				dialog = Gtk.MessageDialog(self.parent, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "This Desktop Is Openbox")
				dialog.format_secondary_text("msettings cant find rc.xml.")
				dialog.run()
				dialog.destroy()
				return False
			else:
				dir_ = os.path.expanduser("~/.config/openbox")
				if not os.path.exists("/etc/xdg/openbox/rc.xml"):
					return False
				if not os.path.exists(dir_):
					os.mkdir(dir_)
				os.system("cp /etc/xdg/openbox/rc.xml '{}'".format(dir_))
		f = open(rc_dir,"r")
		self.xml_root = ET.fromstring(f.read())
		f.close()

		tab_box = Gtk.HBox()
		self.openbox_tabs = Gtk.Notebook()
		self.openbox_tabs.set_scrollable(True)
		tab_box.pack_start(self.openbox_tabs,True,True,5)
		self.add(tab_box)

		#UI TABS UP
		self.resistance_ui_up()
		self.focus_ui_up()
		self.placement_ui_up()
		self.theme_ui_up()
		self.desktop_ui_up()
		self.resize_ui_up()
		#self.keyboard_ui_up()
		#self.mouse_ui_up()
		self.margins_ui_up()
		self.menu_ui_up()
		self.dock_ui_up()



	def yes_no_true_false(self,give):
		if give == "yes":
			return True
		elif give == "no":
			return False
		elif give == True:
			return "yes"
		elif give == False:
			return "no"

#########################################################################
#                           Resistance
#########################################################################
	def resistance_ui_up(self):
		resistance_box = Gtk.VBox()
		self.openbox_tabs.append_page(resistance_box)
		self.openbox_tabs.set_tab_label_text(resistance_box,"Resistance")
		
		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Strength Tells Openbox how much resistance (in pixels) there is between\ntwo windows before it lets them overlap.")
		l_box.pack_start(label,False,False,5)
		resistance_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 100, 1, 0, 0)
		self.strength = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.strength.set_wrap(True)
		l_box.pack_end(self.strength,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Screen_edge_strength Basically the same as strength but between\nwindow and the screen edge.")
		l_box.pack_start(label,False,False,5)
		resistance_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 100, 1, 0, 0)
		self.screen_edge_strength = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.screen_edge_strength.set_wrap(True)
		l_box.pack_end(self.screen_edge_strength,False,False,5)

		#Get resistance writes
		a = xml_setting.get_resistance(self.xml_root)
		self.strength.set_value(int(a["strength"]))
		self.screen_edge_strength.set_value(int(a["screen_edge_strength"]))

		apply_resistance = Gtk.Button()
		apply_resistance.connect("clicked",self.resistance_set)
		apply_resistance.set_label("Apply")
		resistance_box.pack_end(apply_resistance,False,True,5)


	def resistance_set(self,widget):
		strength = self.strength.get_text()
		screen_edge_strength = self.screen_edge_strength.get_text()
		xml_setting.set_resistance(self.xml_root,strength,screen_edge_strength)

#########################################################################
#                               Focus
#########################################################################

	def focus_ui_up(self):
		focus_box = Gtk.VBox()
		self.openbox_tabs.append_page(focus_box)
		self.openbox_tabs.set_tab_label_text(focus_box,"Focus")

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Openbox will automatically give focus to new windows when they are created,\notherwise the focus will stay as it is.")
		l_box.pack_start(label,False,False,5)
		focus_box.pack_start(l_box,False,True,5)

		self.focus_new = Gtk.CheckButton()
		l_box.pack_end(self.focus_new,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Makes focus follow mouse. e.g. when the mouse is being moved the focus will\nbe given to window under the mouse cursor.")
		l_box.pack_start(label,False,False,5)
		focus_box.pack_start(l_box,False,True,5)

		self.focus_last = Gtk.CheckButton()
		l_box.pack_end(self.focus_last,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("When switching desktops, focus the last focused window on that desktop again,\nregardless of where the mouse is. Only applies followMouse is set.")
		l_box.pack_start(label,False,False,5)
		focus_box.pack_start(l_box,False,True,5)

		self.follow_mouse = Gtk.CheckButton()
		l_box.pack_end(self.follow_mouse,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("The time (in milliseconds) Openbox will wait before giving focus to the\nwindow under mouse cursor. Only applies if followMouse is set.")
		l_box.pack_start(label,False,False,5)
		focus_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 500, 1, 0, 0)
		self.focus_delay = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.focus_delay.set_wrap(True)
		l_box.pack_end(self.focus_delay,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Focus windows under the mouse not only when the mouse moves, but also when it\nenters another window due to other reasons (e.g. the window the mouse was in\nmoved/closed/iconified etc). Only applies if followMouse is set")
		l_box.pack_start(label,False,False,5)
		focus_box.pack_start(l_box,False,True,5)

		self.under_mouse = Gtk.CheckButton()
		l_box.pack_end(self.under_mouse,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Also raises windows to top when they are focused. Only applies if followMouse is set.")
		l_box.pack_start(label,False,False,5)
		focus_box.pack_start(l_box,False,True,5)

		self.raise_on_focus = Gtk.CheckButton()
		l_box.pack_end(self.raise_on_focus,False,False,5)


		a = xml_setting.get_focus(self.xml_root)
		self.focus_new.set_active(self.yes_no_true_false(a['focusNew']))
		self.focus_last.set_active(self.yes_no_true_false(a['focusLast']))
		self.follow_mouse.set_active(self.yes_no_true_false(a['followMouse']))
		self.focus_delay.set_value(int(a['focusDelay']))
		self.under_mouse.set_active(self.yes_no_true_false(a['underMouse']))
		self.raise_on_focus.set_active(self.yes_no_true_false(a['raiseOnFocus']))

		apply_resistance = Gtk.Button()
		apply_resistance.connect("clicked",self.focus_set)
		apply_resistance.set_label("Apply")
		focus_box.pack_end(apply_resistance,False,True,5)


	def focus_set(self,widget):
		xml_setting.set_focus(self.xml_root,
							self.yes_no_true_false(self.focus_new.get_active()),
							self.yes_no_true_false(self.focus_last.get_active()),
							self.yes_no_true_false(self.follow_mouse.get_active()),
							self.focus_delay.get_text(),
							self.yes_no_true_false(self.under_mouse.get_active()),
							self.yes_no_true_false(self.raise_on_focus.get_active()))


#########################################################################
#                               Placement
#########################################################################

	def placement_ui_up(self):
		placement_box = Gtk.VBox()
		self.openbox_tabs.append_page(placement_box)
		self.openbox_tabs.set_tab_label_text(placement_box,"Placement")

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("'Smart' will cause new windows to be placed automatically by Openbox.\n'UnderMouse' makes new windows to be placed under the mouse cursor.")
		l_box.pack_start(label,False,False,5)
		placement_box.pack_start(l_box,False,True,5)

		policys = {"Smart":0,"UnderMouse":1}
		self.policy = Gtk.ComboBoxText()
		for pol in policys.keys():
			self.policy.append_text(pol)
		l_box.pack_end(self.policy,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Center can be either yes or no. If it is enabled, windows will open centered\nin the free area found.")
		l_box.pack_start(label,False,False,5)
		placement_box.pack_start(l_box,False,True,5)

		self.placement_center = Gtk.CheckButton()
		l_box.pack_end(self.placement_center,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("With Smart placement on a multi-monitor system, try to place new windows\n on: 'Any' - any monitor, 'Mouse' - where the mouse is, 'Active' - where\nthe active window is, 'Primary' - only on the primary monitor")
		l_box.pack_start(label,False,False,5)
		placement_box.pack_start(l_box,False,True,5)

		monitor_policys = {"Any":0,"Mouse":1,"Active":2,"Primary":3}
		self.monitor_policy = Gtk.ComboBoxText()
		for pol in monitor_policys.keys():
			self.monitor_policy.append_text(pol)
		l_box.pack_end(self.monitor_policy,False,False,5)


		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("The monitor where Openbox should place popup dialogs such as the focus\ncycling popup, or the desktop switch popup. It can be an index from 1,\nspecifying a particular monitor.Or it can be one of the following\n'Mouse'-where the mouse is, or 'Active'-where the active window is")
		l_box.pack_start(label,False,False,5)
		placement_box.pack_start(l_box,False,False,5)

		primarys = {"1":0,"Mouse":1,"Active":2}
		self.primary_monitor = Gtk.ComboBoxText()
		for pri in primarys.keys():
			self.primary_monitor.append_text(pri)
		l_box.pack_end(self.primary_monitor,False,True,5)



		a = xml_setting.get_placement(self.xml_root)
		self.policy.set_active(policys[a["policy"]])
		self.placement_center.set_active(self.yes_no_true_false(a["center"]))
		self.monitor_policy.set_active(monitor_policys[a["monitor"]])
		self.primary_monitor.set_active(primarys[a["primaryMonitor"]])

		apply_placement = Gtk.Button()
		apply_placement.connect("clicked",self.placement_set)
		apply_placement.set_label("Apply")
		placement_box.pack_end(apply_placement,False,True,5)


	def placement_set(self,widget):
		xml_setting.set_placement(self.xml_root,
							self.policy.get_active_text(),
							self.yes_no_true_false(self.placement_center.get_active()),
							self.monitor_policy.get_active_text(),
							self.primary_monitor.get_active_text())


#########################################################################
#                               Theme
#########################################################################

	def get_theme_names(self,dir_):
		themes = []
		for theme in os.listdir(dir_):
			openbox = os.path.join(dir_,theme,"openbox-3")
			if os.path.exists(openbox):
				themes.append(theme)
		themes.sort()
		s = 0
		themess = {}
		for the in themes:
			themess[the] = s
			s += 1
		return themess


	def theme_ui_up(self):
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		theme_box = Gtk.VBox()
		scroll.add(theme_box)
		self.openbox_tabs.append_page(scroll)
		self.openbox_tabs.set_tab_label_text(scroll,"Theme")


		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Name The name of the Openbox theme to use.")
		l_box.pack_start(label,False,False,5)
		theme_box.pack_start(l_box,False,True,5)

		theme_names = self.get_theme_names("/usr/share/themes")
		self.theme_names = Gtk.ComboBoxText()
		for theme in theme_names.keys():
			self.theme_names.append_text(theme)
		l_box.pack_end(self.theme_names,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("TitleLayout tells in which order and what buttons should be in a\nwindow's titlebar. The following letters can be used, each only once\nN :window icon L :window label (aka. title) I: iconify M: maximize\nC: close S: shade (roll up/down) D: omnipresent (on all desktops).")
		l_box.pack_start(label,False,False,5)
		theme_box.pack_start(l_box,False,True,5)

		self.title_layout = Gtk.Entry()
		l_box.pack_end(self.title_layout,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("KeepBorder tells if windows should keep the border drawn by Openbox when window\ndecorations are turned off.")
		l_box.pack_start(label,False,False,5)
		theme_box.pack_start(l_box,False,True,5)

		self.theme_keep_border = Gtk.CheckButton()
		l_box.pack_end(self.theme_keep_border,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("AnimateIconify adds a little iconification animation if enabled. font Specifies the\nfont to use for a specific element of the window. Place can be either of")
		l_box.pack_start(label,False,False,5)
		theme_box.pack_start(l_box,False,True,5)

		self.theme_anime_icon = Gtk.CheckButton()
		l_box.pack_end(self.theme_anime_icon,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Active Window Font")
		l_box.pack_start(label,False,False,5)
		theme_box.pack_start(l_box,False,True,5)

		self.theme_active_font = Gtk.FontButton()
		l_box.pack_end(self.theme_active_font,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Inactive Window Font")
		l_box.pack_start(label,False,False,5)
		theme_box.pack_start(l_box,False,True,5)

		self.theme_inactive_font = Gtk.FontButton()
		l_box.pack_end(self.theme_inactive_font,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Menu Header Font")
		l_box.pack_start(label,False,False,5)
		theme_box.pack_start(l_box,False,True,5)

		self.theme_menu_header_font = Gtk.FontButton()
		l_box.pack_end(self.theme_menu_header_font,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Menu Item Font")
		l_box.pack_start(label,False,False,5)
		theme_box.pack_start(l_box,False,True,5)

		self.theme_menu_item_font = Gtk.FontButton()
		l_box.pack_end(self.theme_menu_item_font,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Active On Screen Display")
		l_box.pack_start(label,False,False,5)
		theme_box.pack_start(l_box,False,True,5)

		self.active_on_display = Gtk.FontButton()
		l_box.pack_end(self.active_on_display,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Inactive On Screen Display")
		l_box.pack_start(label,False,False,5)
		theme_box.pack_start(l_box,False,True,5)

		self.inactive_on_display = Gtk.FontButton()
		l_box.pack_end(self.inactive_on_display,False,False,5)

		apply_placement = Gtk.Button()
		apply_placement.connect("clicked",self.theme_set)
		apply_placement.set_label("Apply")
		theme_box.pack_end(apply_placement,False,True,5)

		a = xml_setting.get_theme(self.xml_root)

		self.theme_names.set_active(theme_names[a["name"]])
		self.title_layout.set_text(a['titleLayout'])
		self.theme_keep_border.set_active(self.yes_no_true_false(a['keepBorder']))
		self.theme_anime_icon.set_active(self.yes_no_true_false(a['animateIconify']))
		for font in a['fonts']:
			if font['place'] == 'ActiveWindow':
				self.theme_active_font.set_font_name(font['name']+" "+font['size'])
			if font['place'] == 'InactiveWindow':
				self.theme_inactive_font.set_font_name(font['name']+" "+font['size'])
			if font['place'] == 'MenuHeader':
				self.theme_menu_header_font.set_font_name(font['name']+" "+font['size'])
			if font['place'] == 'MenuItem':
				self.theme_menu_item_font.set_font_name(font['name']+" "+font['size'])
			if font['place'] == 'ActiveOnScreenDisplay':
				self.active_on_display.set_font_name(font['name']+" "+font['size'])
			if font['place'] == 'InactiveOnScreenDisplay':
				self.inactive_on_display.set_font_name(font['name']+" "+font['size'])


	def theme_set(self,widget):
		title_chars = ["N","L","I","M","C","S","D"]
		g_name = self.theme_names.get_active_text()
		g_titleLayout = self.title_layout.get_text()
		g_keepBorder = self.yes_no_true_false(self.theme_keep_border.get_active())
		g_animateIconify = self.yes_no_true_false(self.theme_anime_icon.get_active())
		g_font = {}
		g_font['ActiveWindow'] = self.font_size_parse(self.theme_active_font.get_font_name())
		g_font['InactiveWindow'] = self.font_size_parse(self.theme_inactive_font.get_font_name())
		g_font['MenuHeader'] = self.font_size_parse(self.theme_menu_header_font.get_font_name())
		g_font['MenuItem'] = self.font_size_parse(self.theme_menu_item_font.get_font_name())
		g_font['ActiveOnScreenDisplay'] = self.font_size_parse(self.active_on_display.get_font_name())
		g_font['InactiveOnScreenDisplay'] = self.font_size_parse(self.inactive_on_display.get_font_name())
		for char in g_titleLayout:
			if char not in title_chars:
				dialog = Gtk.MessageDialog(self.parent, 0, Gtk.MessageType.ERROR, Gtk.ButtonsType.OK, "Error title layout")
				dialog.format_secondary_text("Title layout chars NLIMCSD")
				dialog.run()
				dialog.destroy()
				return False
			else:
				title_chars.remove(char)
		xml_setting.set_theme(self.xml_root,g_name,g_titleLayout,g_keepBorder,g_animateIconify,g_font)


	def font_size_parse(self,text):
		a = text.split()
		size = a[-1]
		font_name = " ".join(a[:-1])
		return {"name":font_name,"size":size}


#########################################################################
#                               Desktop
#########################################################################

	def desktop_ui_up(self):
		a = xml_setting.get_desktop(self.xml_root)
		desktop_box = Gtk.VBox()
		self.openbox_tabs.append_page(desktop_box)
		self.openbox_tabs.set_tab_label_text(desktop_box,"Desktop")

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("PopupTime Time (in milliseconds) to show the popup when switching\ndesktops.Can be set to 0 to disable the popup completely.")
		l_box.pack_start(label,False,False,5)
		desktop_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 3000, 1, 0, 0)
		self.desktop_popup_time = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.desktop_popup_time.set_wrap(True)
		l_box.pack_end(self.desktop_popup_time,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Firstdesk The number of the desktop to use when first started.")
		l_box.pack_start(label,False,False,5)
		desktop_box.pack_start(l_box,False,True,5)

		self.max_desktop_number = int(a['number'])
		ad = Gtk.Adjustment(0, 1, self.max_desktop_number, 1, 0, 0)
		self.desktop_first = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.desktop_first.set_wrap(True)
		l_box.pack_end(self.desktop_first,False,False,5)

		l_box = Gtk.HBox()
		desktop_box.pack_start(l_box,False,True,5)

		add_ws = Gtk.Button()
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_ADD,Gtk.IconSize.BUTTON)
		add_ws.set_label("Add Workspace")
		add_ws.set_image(image)
		add_ws.connect("clicked",self.desktop_add_ws)
		l_box.pack_start(add_ws,False,False,5)

		remove_ws = Gtk.Button()
		image = Gtk.Image()
		image.set_from_stock(Gtk.STOCK_REMOVE,Gtk.IconSize.BUTTON)
		remove_ws.set_label("Remove Workspace")
		remove_ws.set_image(image)
		remove_ws.connect("clicked",self.desktop_remove_ws)
		l_box.pack_start(remove_ws,False,False,5)

		l_box = Gtk.HBox()
		desktop_box.pack_start(l_box,True,True,5)

		self.desktop_store = Gtk.ListStore(str)
		self.desktop_list = Gtk.TreeView(model=self.desktop_store)
		self.desktop_list.set_activate_on_single_click(True)
		renderer = Gtk.CellRendererText()
		coloumn = Gtk.TreeViewColumn("Desktops",renderer, text = 0)
		self.desktop_list.append_column(coloumn)

		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)
		scroll.add(self.desktop_list)
		l_box.pack_start(scroll,True,True,5)

		apply_placement = Gtk.Button()
		apply_placement.connect("clicked",self.desktop_set)
		apply_placement.set_label("Apply")
		desktop_box.pack_end(apply_placement,False,True,5)

		self.desktop_popup_time.set_value(int(a['popupTime']))
		self.desktop_first.set_value(int(a['firstdesk']))
		self.desktop_names = a["names"]
		self.desktop_update_list()


	def desktop_update_list(self):
		self.desktop_store.clear()
		for i in range(1,self.max_desktop_number+1):
			if len(self.desktop_names) < i:
				name = "Workspace {}".format(str(i))
				self.desktop_names.append(name)
			else:
				name = self.desktop_names[i-1]
			self.desktop_store.append([name])			

	def desktop_add_ws(self,widget):
		if self.max_desktop_number < 10:
			self.max_desktop_number += 1
			self.desktop_update_list()

	def desktop_remove_ws(self,widget):
		if self.max_desktop_number > 1:
			self.max_desktop_number -= 1
			self.desktop_names.pop(-1)
			self.desktop_update_list()


	def desktop_set(self,widget):
		xml_setting.set_desktop(self.xml_root,
								str(self.max_desktop_number),
								self.desktop_first.get_text(),
								self.desktop_popup_time.get_text(),
								self.desktop_names)

#########################################################################
#                               Resize
#########################################################################

	def resize_ui_up(self):
		resize_box = Gtk.VBox()
		self.openbox_tabs.append_page(resize_box)
		self.openbox_tabs.set_tab_label_text(resize_box,"Resize")

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Resize the program inside the window while resizing. When disabled the\nunused space will be filled with a uniform color during a resize.")
		l_box.pack_start(label,False,False,5)
		resize_box.pack_start(l_box,False,True,5)

		self.resize_draw_contenst = Gtk.CheckButton()
		l_box.pack_end(self.resize_draw_contenst,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("When to show the move/resize popup. Always always shows it, Never never\nshows it, Nonpixel shows it only when resizing windows that have specified\nthey are resized in increments larger than one pixel, usually terminals.")
		l_box.pack_start(label,False,False,5)
		resize_box.pack_start(l_box,False,True,5)

		popups_show = {"Always":0,"Never":1,"Nonpixel":2}
		self.resize_popup_show = Gtk.ComboBoxText()
		for a in popups_show.keys():
			self.resize_popup_show.append_text(a)
		l_box.pack_end(self.resize_popup_show,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Where to show the popup. Top shows the popup above the titlebar of the\nwindow. Center shows it centered on the window. Fixed shows it in a\nfixed location on the screen specified by popupFixedPosition.")
		l_box.pack_start(label,False,False,5)
		resize_box.pack_start(l_box,False,True,5)

		popups_position = {"Top":0,"Center":1,"Fixed":2}
		self.resize_popup_position = Gtk.ComboBoxText()
		for a in popups_position.keys():
			self.resize_popup_position.append_text(a)
		l_box.pack_end(self.resize_popup_position,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Specifies where on the screen show the position when\nFixed. Both x and y take coordinates as described here.")
		l_box.pack_start(label,False,False,5)
		resize_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 1920, 1, 0, 0)
		self.resize_x = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.resize_x.set_wrap(True)
		l_box.pack_start(self.resize_x,False,False,5)

		ad = Gtk.Adjustment(0, 0, 1080, 1, 0, 0)
		self.resize_y = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.resize_y.set_wrap(True)
		l_box.pack_end(self.resize_y,False,False,5)

		a = xml_setting.get_resize(self.xml_root)
		self.resize_draw_contenst.set_active(self.yes_no_true_false(a['drawContents']))
		self.resize_popup_show.set_active(popups_show[a['popupShow']])
		self.resize_popup_position.set_active(popups_position[a['popupPosition']])
		self.resize_x.set_value(int(a['pos_x']))
		self.resize_y.set_value(int(a['pos_y']))

		apply_placement = Gtk.Button()
		apply_placement.connect("clicked",self.resize_set)
		apply_placement.set_label("Apply")
		resize_box.pack_end(apply_placement,False,True,5)

	def resize_set(self,widget):
		xml_setting.set_resize(self.xml_root,
								self.yes_no_true_false(self.resize_draw_contenst.get_active()),
								self.resize_popup_show.get_active_text(),
								self.resize_popup_position.get_active_text(),
								self.resize_x.get_text(),
								self.resize_y.get_text())



#########################################################################
#                               Keyboard
#########################################################################

	def keyboard_ui_up(self):
		keyboard_box = Gtk.VBox()
		self.openbox_tabs.append_page(keyboard_box)
		self.openbox_tabs.set_tab_label_text(keyboard_box,"Keyboard")

		xml_setting.get_keyboard(self.xml_root)


#########################################################################
#                               Mouse
#########################################################################

	def mouse_ui_up(self):
		mouse_box = Gtk.VBox()
		self.openbox_tabs.append_page(mouse_box)
		self.openbox_tabs.set_tab_label_text(mouse_box,"Mouse")

#########################################################################
#                               Margins
#########################################################################

	def margins_ui_up(self):
		margins_box = Gtk.VBox()
		self.openbox_tabs.append_page(margins_box)
		self.openbox_tabs.set_tab_label_text(margins_box,"Margins")

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Each tag specifies the amount of pixels to reserve at the respective edge of the screen.\nNew windows will not be placed in those areas, and maximized windows will not cover them.")
		l_box.pack_start(label,False,False,5)
		margins_box.pack_start(l_box,False,True,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Top")
		l_box.pack_start(label,False,False,5)
		margins_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 1920, 1, 0, 0)
		self.margin_top = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.margin_top.set_wrap(True)
		l_box.pack_end(self.margin_top,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Bottom")
		l_box.pack_start(label,False,False,5)
		margins_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 1920, 1, 0, 0)
		self.margin_bottom = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.margin_bottom.set_wrap(True)
		l_box.pack_end(self.margin_bottom,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Left")
		l_box.pack_start(label,False,False,5)
		margins_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 1920, 1, 0, 0)
		self.margin_left = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.margin_left.set_wrap(True)
		l_box.pack_end(self.margin_left,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Right")
		l_box.pack_start(label,False,False,5)
		margins_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 1920, 1, 0, 0)
		self.margin_right = Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.margin_right.set_wrap(True)
		l_box.pack_end(self.margin_right,False,False,5)

		apply_placement = Gtk.Button()
		apply_placement.connect("clicked",self.margins_set)
		apply_placement.set_label("Apply")
		margins_box.pack_end(apply_placement,False,True,5)


		a = xml_setting.get_margins(self.xml_root)
		self.margin_top.set_value(int(a["top"]))
		self.margin_bottom.set_value(int(a["bottom"]))
		self.margin_left.set_value(int(a["left"]))
		self.margin_right.set_value(int(a["right"]))

	def margins_set(self,widget):
		xml_setting.set_margins(self.xml_root,
								self.margin_top.get_text(),
								self.margin_bottom.get_text(),
								self.margin_left.get_text(),
								self.margin_right.get_text())


#########################################################################
#                               Menu
#########################################################################

	def menu_ui_up(self):
		menu_box = Gtk.VBox()
		self.openbox_tabs.append_page(menu_box)
		self.openbox_tabs.set_tab_label_text(menu_box,"Menu")

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("How long (in milliseconds) you have to hold the mouse button down for\nit to be hidden automatically when you release it. If you hold shorter,\nit will stay up when you release.")
		l_box.pack_start(label,False,False,5)
		menu_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 3000, 1, 0, 0)
		self.menu_hide_delay =  Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.menu_hide_delay.set_wrap(True)
		l_box.pack_end(self.menu_hide_delay,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Position menus centered vertically instead of aligned to the top.")
		l_box.pack_start(label,False,False,5)
		menu_box.pack_start(l_box,False,True,5)

		self.menu_middle = Gtk.CheckButton()
		l_box.pack_end(self.menu_middle,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Submenu Show Delay")
		l_box.pack_start(label,False,False,5)
		menu_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 3000, 1, 0, 0)
		self.submenu_show_delay =  Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.submenu_show_delay.set_wrap(True)
		l_box.pack_end(self.submenu_show_delay,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Submenu Hide Delay")
		l_box.pack_start(label,False,False,5)
		menu_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 3000, 1, 0, 0)
		self.submenu_hide_delay =  Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.submenu_hide_delay.set_wrap(True)
		l_box.pack_end(self.submenu_hide_delay,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Whether to show window icons in the Desktop and Windows menus\n(client-list-menu and client-list-combined-menu).")
		l_box.pack_start(label,False,False,5)
		menu_box.pack_start(l_box,False,True,5)

		self.menu_show_icon = Gtk.CheckButton()
		l_box.pack_end(self.menu_show_icon,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Whether to show the Add new desktop and Remove last desktop entries\nin the Desktop and Windows menus.")
		l_box.pack_start(label,False,False,5)
		menu_box.pack_start(l_box,False,True,5)

		self.menu_manage_desktop = Gtk.CheckButton()
		l_box.pack_end(self.menu_manage_desktop,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Specify files to load menu specifications from. Can be given more than\nonce, although care should be taken to avoid id clashes. Files are\nsearched for in the user directory first and then in the system directory.")
		l_box.pack_start(label,False,False,5)
		menu_box.pack_start(l_box,False,True,5)

		self.menu_file = Gtk.FileChooserButton()
		l_box.pack_end(self.menu_file,False,False,5)

		apply_placement = Gtk.Button()
		apply_placement.connect("clicked",self.menu_set)
		apply_placement.set_label("Apply")
		menu_box.pack_end(apply_placement,False,True,5)

		a = xml_setting.get_menu(self.xml_root)
		self.menu_hide_delay.set_value(int(a['hideDelay']))
		self.menu_middle.set_active(self.yes_no_true_false(a['middle']))
		self.submenu_hide_delay.set_value(int(a['submenuHideDelay']))
		self.submenu_show_delay.set_value(int(a['submenuShowDelay']))
		self.menu_show_icon.set_active(self.yes_no_true_false(a['applicationIcons']))
		self.menu_manage_desktop.set_active(self.yes_no_true_false(a['manageDesktops']))
		self.menu_file.set_filename(a["file"])

	def menu_set(self,widget):
		xml_setting.set_menu(self.xml_root,
							self.menu_hide_delay.get_text(),
							self.yes_no_true_false(self.menu_middle.get_active()),
							self.submenu_hide_delay.get_text(),
							self.submenu_show_delay.get_text(),
							self.yes_no_true_false(self.menu_show_icon.get_active()),
							self.yes_no_true_false(self.menu_manage_desktop.get_active()),
							self.menu_file.get_filename())


#########################################################################
#                               Dock
#########################################################################

	def dock_ui_up(self):
		scroll = Gtk.ScrolledWindow()
		scroll.set_policy(Gtk.PolicyType.AUTOMATIC, Gtk.PolicyType.AUTOMATIC)

		dock_box = Gtk.VBox()
		scroll.add(dock_box)

		self.openbox_tabs.append_page(scroll)
		self.openbox_tabs.set_tab_label_text(scroll,"Dock")

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Specify where to show the dock. Can be one of TopLeft, Top, TopRight,\nRight, BottomRight, Bottom, BottomLeft, Left and Floating.")
		l_box.pack_start(label,False,False,5)
		dock_box.pack_start(l_box,False,True,5)

		dock_pos = {"TopLeft":0,"Top":1,"TopRight":2,"Right":3,"BottomRight":4,"Bottom":5,"BottomLeft":6,"Left":7}
		self.dock_position = Gtk.ComboBoxText()
		for p in dock_pos.keys():
			self.dock_position.append_text(p)
		l_box.pack_end(self.dock_position,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Which window layer to put the dock in. Can be one of above, normal,\nbelow. The dock can be raised and lowered by left and middle\nclicking on it, among windows in the specified layer.")
		l_box.pack_start(label,False,False,5)
		dock_box.pack_start(l_box,False,True,5)

		dock_stacking = {"Above":0,"Normal":1,"Below":2}
		self.dock_stacking = Gtk.ComboBoxText()
		for p in dock_stacking.keys():
			self.dock_stacking.append_text(p)
		l_box.pack_end(self.dock_stacking,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Specify if dockapps should be laid out in a Vertical or Horizontal direction.")
		l_box.pack_start(label,False,False,5)
		dock_box.pack_start(l_box,False,True,5)

		dock_direction = {"Vertical":0,"Horizontal":1}
		self.dock_direction = Gtk.ComboBoxText()
		for p in dock_direction.keys():
			self.dock_direction.append_text(p)
		l_box.pack_end(self.dock_direction,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("When position is set to Floating, these specify the pixel position of the dock. Cannot\ncurrently use the slightly extended format described at the bottom of this page.")
		l_box.pack_start(label,False,False,5)
		dock_box.pack_start(l_box,False,True,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Floating X")
		l_box.pack_start(label,False,False,5)
		dock_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 1920, 1, 0, 0)
		self.floating_x =  Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.floating_x.set_wrap(True)
		l_box.pack_start(self.floating_x,False,False,5)

		label = Gtk.Label()
		label.set_text("Floating Y")
		l_box.pack_start(label,False,False,5)

		ad = Gtk.Adjustment(0, 0, 1080, 1, 0, 0)
		self.floating_y =  Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.floating_y.set_wrap(True)
		l_box.pack_start(self.floating_y,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Whether to hide the dock automatically when the mouse is not over it.")
		l_box.pack_start(label,False,False,5)
		dock_box.pack_start(l_box,False,True,5)

		self.dock_auto_hide = Gtk.CheckButton()
		l_box.pack_end(self.dock_auto_hide,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Specify (in milleseconds) the delays for hiding and showing the dock\nwhen the mouse leaves and enters it, respectively.")
		l_box.pack_start(label,False,False,5)
		dock_box.pack_start(l_box,False,True,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Hide Delay")
		l_box.pack_start(label,False,False,5)
		dock_box.pack_start(l_box,False,True,5)

		ad = Gtk.Adjustment(0, 0, 3000, 1, 0, 0)
		self.menu_hide_delay =  Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.menu_hide_delay.set_wrap(True)
		l_box.pack_start(self.menu_hide_delay,False,False,5)

		label = Gtk.Label()
		label.set_text("Show Delay")
		l_box.pack_start(label,False,False,5)

		ad = Gtk.Adjustment(0, 0, 3000, 1, 0, 0)
		self.menu_show_delay =  Gtk.SpinButton(adjustment=ad, climb_rate=1, digits=0)
		self.menu_show_delay.set_wrap(True)
		l_box.pack_start(self.menu_show_delay,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Specifies which button to use for moving individual dockapps around in the\ndock, see the bindings page for details.")
		l_box.pack_start(label,False,False,5)
		dock_box.pack_start(l_box,False,True,5)

		dock_move_button = {"Left":0,"Right":1,"Middle":2,"Up":3,"Down":4,"Button6":5,"Button7":6}
		self.dock_move_button = Gtk.ComboBoxText()
		for p in dock_move_button.keys():
			self.dock_move_button.append_text(p)
		l_box.pack_end(self.dock_move_button,False,False,5)

		l_box = Gtk.HBox()
		label = Gtk.Label()
		label.set_text("Specifies that the dock should not set a strut, which means it won't stop\nwindows from being placed or maximized over it.")
		l_box.pack_start(label,False,False,5)
		dock_box.pack_start(l_box,False,True,5)

		self.dock_no_strot = Gtk.CheckButton()
		l_box.pack_end(self.dock_no_strot,False,False,5)

		apply_placement = Gtk.Button()
		apply_placement.connect("clicked",self.dock_set)
		apply_placement.set_label("Apply")
		dock_box.pack_end(apply_placement,False,True,5)


		a = xml_setting.get_dock(self.xml_root)
		self.dock_position.set_active(dock_pos[a['position']])
		self.dock_stacking.set_active(dock_stacking[a['stacking']])
		self.dock_direction.set_active(dock_direction[a['direction']])
		self.floating_x.set_value(int(a['floatingX']))
		self.floating_y.set_value(int(a['floatingY']))
		self.dock_auto_hide.set_active(self.yes_no_true_false(a['autoHide']))
		self.menu_hide_delay.set_value(int(a['hideDelay']))
		self.menu_show_delay.set_value(int(a['showDelay']))
		self.dock_move_button.set_active(dock_move_button[a['moveButton']])
		self.dock_no_strot.set_active(self.yes_no_true_false(a['noStrut']))

	def dock_set(self,widget):
		xml_setting.set_dock(self.xml_root,
							self.dock_position.get_active_text(),
							self.dock_stacking.get_active_text(),
							self.dock_direction.get_active_text(),
							self.floating_x.get_text(),
							self.floating_y.get_text(),
							self.yes_no_true_false(self.dock_auto_hide.get_active()),
							self.menu_hide_delay.get_text(),
							self.menu_show_delay.get_text(),
							self.dock_move_button.get_active_text(),
							self.yes_no_true_false(self.dock_no_strot.get_active()))








