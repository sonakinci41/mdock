
import subprocess,gi
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk



class Calendar(Gtk.Window):
	def __init__(self):
		Gtk.Window.__init__(self)
		self.connect("focus-out-event",self.focus_change)
		self.set_type_hint(Gdk.WindowTypeHint.UTILITY)
		self.set_decorated(False)

		m_box = Gtk.VBox()
		self.add(m_box)

		self.calendar = Gtk.Calendar()
		m_box.pack_start(self.calendar,True,True,0)

	def focus_change(self,window,event):
		self.destroy()

	def set_menu_position(self,pos_x,pos_y,dock_position,cursor,icon):
		size = self.get_size()
		height = size.height
		width = size.width
		if pos_x == 0:
			x = dock_position[2] + 20
			y = dock_position[1] + cursor[1] - height / 2 + icon / 2
			#self.h_scale.set_inverted(True)
		elif pos_x == 2:
			print(dock_position[0],height)
			x = dock_position[0]-width - 20
			y = dock_position[1] + cursor[1] - height / 2 + icon / 2
			#self.h_scale.set_inverted(True)

		if pos_y == 0:
			x = dock_position[0] + cursor[0] - width / 2 + icon / 2
			y = dock_position[3] + 20
			#self.h_scale.set_inverted(False)
		elif pos_y == 2:
			x = dock_position[0] + cursor[0] - width / 2 + icon / 2
			y = dock_position[1] - height - 20
			#self.h_scale.set_inverted(True)#Yukardan aşağı aşağıdan yukarı

		self.move(x,y)
		self.show_all()

if __name__ == '__main__':
	pen = Calendar()
	pen.show_all()
	Gtk.main()
